<section class="row">
	<div class="col-md-6 col-md-offset-3">

		@if($errors->any())
			<div class="alert alert-danger" role="alert">
		        <ul>
		            @foreach($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		@if (Session::has('message'))
			<div class="alert alert-success" role="alert">
				{{ Session('message') }}
			</div>
		@endif

		@if (Session::has('messageFail'))
			<div class="alert alert-danger" role="alert">
				{{ Session('messageFail') }}
			</div>
		@endif

	</div>
</section>