@extends('base')

@section('title')
	Welcome!
@endsection


@section('content')

	@include('common.message-block')


	<div class="row">

		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3>Sign Up</h3></div>
				<div class="panel-body">
					<form action="{{ route('signup') }}" method="post">
						<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
							<label for="email">Your Email</label>
							<input class="form-control" type="text" name="email" id="email" value="{{ old('email') }}">
						</div>
						<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							<label for="name">Your name</label>
							<input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
						</div>
						<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
							<label for="password">Your password</label>
							<input class="form-control" type="password" name="password" id="password" value="{{ old('password') }}">
						</div>
						<input type="submit" class="btn btn-primary" value="Submit">
						<input type="hidden" name="_token" value="{{ Session::token() }}">
					</form>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3>Sign In</h3></div>
				<div class="panel-body">
					<form action="{{ route('signin') }}" method="post">
						<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
							<label for="email">Your Email</label>
							<input class="form-control" type="text" name="email" id="email" value="{{ old('email') }}">
						</div>
						<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
							<label for="password">Your password</label>
							<input class="form-control" type="password" name="password" id="password" value="{{ old('password') }}">
						</div>
						<input type="submit" class="btn btn-primary" value="Submit">
						<input type="hidden" name="_token" value="{{ Session::token() }}">
					</form>
				</div>
			</div>
		</div>

	</div>

@endsection