@extends('base')

@section('title')
	Socket
@endsection


@section('content')

	@if (Session::has('message'))
		<div class="alert alert-success" role="alert">{{ Session('message') }}</div>
	@endif

	<h1>Chat socket</h1>

	<button onclick="send();" class="btn btn-primary">Send</button>


	<script>
		var conn = new WebSocket('ws://sonet.lc:8080');
		conn.onopen = function (e) {
			console.log("Connection estabilished!");
		}

		conn.onmessage = function (e) {
			console.log('Получены данные: ' + e.data);
		}

		function send() {
			var data = 'Данные для отправки: ' + Math.random();
			conn.send(data);
			console.log('Отправлено: ' + data);
		}
		
	</script>

@endsection