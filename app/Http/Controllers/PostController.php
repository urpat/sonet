<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Post;
use App\Model\User;
use App\Model\Like;

class PostController extends Controller
{
    public function getDashboard(Request $request)
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        return view('dashboard', ['posts' => $posts]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'body' => 'required|max:1000'
        ]);

        $post = new Post();
        $post->body = $request->body;

        $message = 'Error save post';
        if ($request->user()->post()->save($post))
        {
            $message = 'Post successfully created!';
        }

        $request->session()->flash('message', $message);
    }

    public function getPosts()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        return response()->json(['posts' => $posts], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'body' => 'required'
        ]);

        $post = Post::find($request->postId);

        if (Auth::user() != $post->user)
        {
            return redirect()->back();
        }

        $post->body = $request->body;
        $post->update();

        return response()->json(['newBody' => $post->body], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($post_id)
    {
        $post = Post::find($post_id);

        if (Auth::user() != $post->user)
        {
            return redirect()->back();
        }

        $post->delete();
        return redirect()->back()->with(['message' => 'Success delete!']);
    }

    public function like(Request $request)
    {
        $postId = $request->postId;
        $isLike = $request->isLike === 'true';
        $update = false;

        $post = Post::find($postId);

        if (!$post)
        {
            return null;
        }

        $user = Auth::user();
        $like = $user->like()->where('post_id', $postId)->first();

        if ($like)
        {
            $alreadyLike = $like->like;
            $update = true;

            if ($alreadyLike == $isLike)
            {
                $like->delete();
                return null;
            }
        }
        else 
        {
            $like = new Like();
        }

        $like->like = $isLike;
        $like->user_id = $user->id;
        $like->post_id = $post->id;

        if ($update)
        {
            $like->update();
        }
        else
        {
            $like->save();
        }

        return null;
    }
}
