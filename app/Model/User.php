<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Contracts\Auth\Authenticatable;

class User extends Model implements Authenticatable
{
	use \Illuminate\Auth\Authenticatable;

    protected $table = 'user';

    protected $fillable = ['name', 'email', 'created_at', 'updated_at', 'password'];

    public function post()
    {
    	return $this->hasMany('App\Model\Post');
    }

    public function like()
    {
    	return $this->hasMany('App\Model\Like');
    }
}
