<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/




Route::get('socket', function () {
    return view('socket');
});






Route::group(['middleware' => ['web']], function () {

	Route::get('/', function () {
    	return view('welcome');
	})->name('home');

	Route::post('signup',   ['as' => 'signup', 'uses' => 'UserController@postSignUp']);
	Route::post('signin',   ['as' => 'signin', 'uses' => 'UserController@postSignIn']);
	Route::get('logout',   ['as' => 'logout', 'uses' => 'UserController@logout']);

	Route::get('account',   ['as' => 'account', 'uses' => 'UserController@show']);
	Route::get('account/image/{filename}',   ['as' => 'account.image', 'uses' => 'UserController@accountImage']);
	Route::post('account/save',   ['as' => 'account.save', 'uses' => 'UserController@save']);

	Route::get('dashboard', [
		'middleware' => 'auth',
		'as'         => 'dashboard',
		'uses'       => 'PostController@getDashboard'
	]);

	Route::get('getPosts', [
		'middleware' => 'auth',
		'as'         => 'getPosts',
		'uses'       => 'PostController@getPosts'
	]);

	Route::post('post/create', [
		'middleware' => 'auth',
		'as'         => 'post.create',
		'uses'       => 'PostController@create'
	]);

	Route::post('edit', [
		'uses' => 'PostController@update',
		'as'   => 'edit'
	]);
	
	Route::get('post/delete/{post_id}', [
		'middleware' => 'auth',
		'as'         => 'post.delete',
		'uses'       => 'PostController@delete'
	]);

	Route::post('like', [
		'uses' => 'PostController@like',
		'as' => 'like'
	]);

});
