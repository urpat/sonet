getPosts();

var postId = 0;
var postBodyElement = null;

$('.post').find('.interaction').find('.edit').on('click', function(event) {
	event.preventDefault();

	postBodyElement = event.target.parentNode.parentNode.childNodes[1];
	var postBody = postBodyElement.textContent;
	postId = event.target.parentNode.parentNode.dataset['postid'];
	$('#post-body').val(postBody);
	$('#edit-modal').modal();
});

$('#modal-save').on('click', function() {
	$.ajax({
		method: 'POST',
		url: urlEdit,
		data: {body: $('#post-body').val(), postId: postId, _token: token}
	})
	.done(function (msg) {
		$(postBodyElement).text(msg['newBody']);
		$('#edit-modal').modal('hide');
	});
});

$('.like').on('click', function(event) {
	event.preventDefault();
	postId = event.target.parentNode.parentNode.dataset['postid'];

	var isLike = event.target.previousElementSibling == null;

	$.ajax({
		method: 'POST',
		url: urlLike,
		data: {isLike: isLike, postId: postId, _token: token}
	})
	.done(function() {
		event.target.innerText = isLike ? event.target.innerText == 'Like' ? 'You like this post' : 'Like' : event.target.innerText == 'Dislike' ? 'You dislike this post' : 'Dislike';

		if (isLike) {
			event.target.nextElementSibling.innerText = 'Dislike';
		} else {
			event.target.previousElementSibling.innerText = 'Like';
		}
	});
	
});

$('#create-post').on('click', function() {
	$.ajax({
		method: 'POST',
		url: urlCreate,
		data: {body: $('#new-post').val(), _token: token}
	})
	.done(function () {
		getPosts();
	});
});


function getPosts() {
	$.ajax({
		method: 'GET',
		url: urlGetPosts,
		data: {_token: token}
	})
	.done(function (posts) {
		publicPosts(posts);
	});
};


function publicPosts(posts) {

	ul = document.getElementById("list");

    while (ul.firstChild) {
	  //удалить его
	  ul.removeChild(ul.firstChild);
	}

    $.each(posts, function () {
    	$.each(this, function () {

		    var li = document.createElement("li");

		    var textSmart= document.createTextNode(this.body);

		    li.appendChild(textSmart);

		    ul.appendChild(li);

		});
    });
};

setInterval(getPosts, 300);